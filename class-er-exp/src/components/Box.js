import styled from 'styled-components';
import { space, color, layout, flexbox, grid, padding,  border, width, display,  margin } from 'styled-system';
import propTypes from '@styled-system/prop-types'

const Box= styled.div(
    {
        boxSizing:'border-box',
        
    },
    space,
    color,
    padding,
    border,
    layout,
    flexbox,
    grid,
    width,
    margin
);
Box.propTypes = {
  ...propTypes.space,
    ...propTypes.color, 
    ...propTypes.grid,
    ...propTypes.border,
    ...propTypes.layout,
    ...propTypes.flexbox,
    ...propTypes.margin
}
export default Box;