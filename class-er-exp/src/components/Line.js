import styled from 'styled-components';
import { space, color,  margin, height, maxHeight ,border, verticalAlign} from 'styled-system';
import propTypes from '@styled-system/prop-types'

const Line=styled.div({
boxSizing: 'border-box',


},
space,
color,
height,
maxHeight,
margin,
border,
verticalAlign

);
Line.propTypes={
    ...propTypes.space,
    ...propTypes.color,
    ...propTypes.height,
    ...propTypes.maxHeight,
    ...propTypes.margin,
    ...propTypes.border,
}
export default Line;