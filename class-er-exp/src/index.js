import React from 'react';
import ReactDOM from 'react-dom/client';
import Box from './components/Box';
import Line from './components/Line';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
 <h3>Car Rental Application</h3>

  <Box
  border={'2px solid black'}
    padding={["1.5%"]}
    marginTop={["5%"]}
    width={["8%"]}
     marginLeft={["45%"]}

 >

 </Box>
 <Box
 border={'2px solid black'}
 padding={["1.5%"]}
marginTop={["-3.3%"]}
width={["8%"]}
marginLeft={["33%"]}

></Box>
<Box
 border={'2px solid black'}
 padding={["1.5%"]}
marginTop={["-3.3%"]}
width={["8%"]}
marginLeft={["20.5%"]}
>

</Box>
<Box
 border={'2px solid black'}
 padding={["1.5%"]}
marginTop={["-3.3%"]}
width={["8%"]}
marginLeft={["9%"]}
>

</Box>
<Box
 border={'2px solid black'}
 padding={["1.5%"]}
marginTop={["18%"]}
width={["10%"]}
marginLeft={["9%"]}>Front Office</Box>

<Box border={'2px solid black'}
 padding={["1.5%"]}
marginTop={["-4.7%"]}
width={["10%"]}
marginLeft={["25%"]}>Customer</Box>

<Box border={'2px solid black'}
 padding={["1.5%"]}
marginTop={["-4.7%"]}
width={["10%"]}
marginLeft={["40%"]}>Vehicle</Box>

<Box border={'2px solid black'}
 padding={["1.5%"]}
marginTop={["-4.7%"]}
width={["10%"]}
marginLeft={["55%"]}>Destination</Box>
{/* <Line
height={["4%"]}
border={'0.5px solid black'}

></Line> */}
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();