import React, { Component } from 'react';
import '../Css/Er.css'

class Dropdown extends Component {
  constructor(props) {
    super(props);




    this.state = { value: "coconut" };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  

  handleSubmit(event) {
    alert("Your favorite flavor is: " + this.state.value);
    event.preventDefault();
  }

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  render() {
    return (
      <div>
        <div class="ele">
        <h2>Elements</h2>
      <form onSubmit={this.handleSubmit}>
        <label>
          Table1</label>
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="model">carmodel</option>
            <option value="chas">carchasis</option>
            <option value="id">customer_id</option>
            <option value="name">customer_name</option>
            <option value="rname">renter_name</option>
            <option value="details">ID_card details</option>
            <option value="age">Legal age</option>
            
          </select>

          <label>
          Table2</label>
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="rna">Renter_name</option>
            <option value="idde">ID_Card details</option>
            <option value="lage">Legal age</option>
            <option value="cmodel">car model</option>
            <option value="chasi">carchasis</option>
           
            
          </select>

          <label>
          Table3</label>
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="sname">Staff_name</option>
            <option value="ckey">Car_keys</option>
            <option value="rate">Rating</option>
            
          </select>

          <label>
          Table4</label>
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="tid">Transaction_id</option>
            <option value="amdetail">Amount_details</option>
            <option value="redetail">Refund_details</option>
         
          </select>

        
      </form>
      </div>





      </div>
    );
  }
  }




export default Dropdown;