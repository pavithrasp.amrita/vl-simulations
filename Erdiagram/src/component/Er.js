import React from 'react'
import '../Css/Er.css'


function Er() {
  return (
    <div>
        <div id="app" class="h-100">
         <div id="app3" div class="ER-diagram">
                <div class="ER-diagram-top-header">Try it out</div>
                <div id="app4"div class="ER-diagram-body">
          <div class="container-fluid">
           
                            <h3>ER Diagram</h3>
                        
                            
                              <div id="rtr">
                                    </div> 
                                 
                            <div class="col-elements">
                                <div class="row">  
                                <div class="col-12 col-sm-6 col-md-4">

                                            <div id="card1">
                                                <h6 class="card-header">CAR </h6>
                                                <div class="card-body">
                                                    <ul>
                            
                                                        <li>Carname</li>
                                                        
                                                        <li id="item1"></li>
                                                        <li id="item2"></li>
                                                        <li id="item3"></li>
                                                        <li id="item4"></li>
                                                     
                                                    </ul>
                                                </div>
                                            </div> 
                                         
                                    
                                          
                                            <div id="card2">
                                                <h6 class="card-header">RENTER</h6>  
                                                <div class="card-body">
                                                    <ul>
                                                        <li>Rent_id</li>
                                                       
                                                        <li id="class1"></li>
                                                        <li id="class2"></li>
                                                    
                                                        <li id="class4"></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>                                            

                                        
                                            <div id="card3" >
                                                <h6 class="card-header">STAFF</h6>
                                                <div class="card-body">
                                                    <ul>
                                                        <li>Staff_id</li>
                                                        
                                                        <li id="class31"></li>
                                                        <li id="class32"></li>
                                                        <li id="class33"></li>
                                                        
                                                    </ul>
                                                </div>
                                            </div>    
                                            
                                            <div class="col-12 mb-4 col-lg-4 "></div>
                                        <div class="col-12 col-sm-6 col-md-4">

                                            <div id="card4">
                                                <h6 class="card-header">PAYMENT</h6>
                                                <div class="card-body">
                                                    <ul>
                                                        <li>Transaction_method</li>

                                                        <li id="class41"></li>
                                                        <li id="class42"></li>
                                                        <li id="class43"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                   
                                            </div>
                                        </div>
                               </div>


                               <div class="col-md-3 col-lg-3 col-elements">
                              
                                {/* <ul class="elements">
                                    <li>
                                        <div class="dropdown">
                                            <button class="btn btn-block btn-success dropdown-toggle" type="button"
                                                id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Table1
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2" >
                                            
                                                <button  class="dropdown-item" type="button" id="entity1" value="carmodel" >carmodel</button>
                                                <button  class="dropdown-item" type="button" id="entity2" value="carchasis" >carchasis</button>
                                                <button  class="dropdown-item" type="button" id="error1" value="customer_id" >customer_id</button>
                                                <button  class="dropdown-item" type="button" id="error2" value="customer_name" >customer_name</button>
                                                <button  class="dropdown-item" type="button" id="error3" value="renter_name">renter_name</button>
                                                <button  class="dropdown-item" type="button" id="error4" value="ID_card details">ID_card details</button>
                                                <button  class="dropdown-item" type="button" id="error5" value="Legal age">Legal age</button>  
                                              
                                    
                                            </div>
                                        </div>

                                        
                                    </li>
                                    </ul> */}


                            </div>


                            <div class="ER-diagram-footer">
                         <button type="button" class="btn btn-secondary"  value="Print" onclick="printDiv()">Print  </button>
                   </div>
                        </div>
   
              </div>
</div>
    
</div>
    
</div>
    </div>
   

   
  )
}



export default Er
