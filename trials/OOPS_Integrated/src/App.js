import React from "react";
import Navbar from "./components/Navbar";
import { Switch, Route } from "react-router-dom";
import Jkl from "./components/Jkl";
import Quiz1 from "./components/Quiz1";
import Noun from "./components/Noun"

const Home = () => {
  return (
    <>
      <Navbar />
      <section className="hero-section">
        <p>Introduction to</p>
        <h1>Experiment 1</h1>
      </section>
    </>
  );
};




const Theory = () => {
  return (
    <>
      <Navbar />
      <section className="hero-section">
        <p>Theory</p>
        {/* <h1>Thapa Technical About Page</h1> */}
      </section>
    </>
  );
};

const Simulation = () => {
  return (
    <>
      <Navbar />
      <section className="hero-section">

        {/* <h1>Thapa Technical Service Page</h1> */}
        <Jkl />
        
      </section>
    </>
  );
};

const Quiz = () => {
  return (
    <>
      <Navbar />
      <section className="hero-section">
    <Quiz1 />
      </section>
      
    </>
  );
};

const App = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home/>
      </Route>

      <Route path="/theory">
        <Theory/>
      </Route>

      <Route path="/simulation">
        <Simulation/>
      </Route>

      <Route path="/quiz">
        <Quiz/>
      </Route>
      <Route path="/noun"><Noun /></Route>
    </Switch>
  );
};

export default App;
