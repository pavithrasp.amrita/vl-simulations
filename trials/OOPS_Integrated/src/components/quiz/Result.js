import React from 'react';
import './q.css'

const Result = ({ showResult, quizs, marks, startOver }) => {
    return (
        <section className="text-black" style={{ display: `${showResult ? 'block' : 'none'}` }}>
          
                           
                            <h3 className='mb-3 fw-bold'>Your score is {marks} out of {quizs.length}</h3>

                           
        </section>
    );
};

export default Result;