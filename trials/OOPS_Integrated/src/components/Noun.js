// import { Hide } from 'devextreme-react/color-box';
// import { indexOf } from 'lodash';
import React, { useState } from 'react';
import { GiHidden } from 'react-icons/gi';
import Navbar from './Navbar';
import './noun.css';

import { Message } from 'rsuite';
import { indexOf } from 'lodash';
// import { nounArrays } from './nounarray';
function Component1() {
  // all array is for all words
  // let allarray=['Customer','contract','acquire_a_vehicle','clerk','locates_the_vehicle','reservation','vehicle_type_is_not_available','signs_the_contract','vehicle','marks the contract active','by'];
  const[shownc,setshownc]=useState(false);
  const[showncusn1,setshowncusn1]=useState(false);
  const[shownv,setshownv]=useState(false);
  const[showncl,setshowncl]=useState(false);
  const[showncln,setshowncln]=useState(false);
  const[shownreserv,setshownreserv]=useState(false);
  const[shownvb1,setshownvb1]=useState(false);
  const[shownvb,setshownvb]=useState(false);
  const[shownvrd,setshownvrd]=useState(false);
  const[showncusm,setshowncusm]=useState(false);
  const[msg,setmsg]=useState(false);

  const [msgg,setmsgg]=useState(false);

  let noun=['Customer','acquire a vehicle','clerk','locates the vehicle','reservation','vehicle type is not available','signs the contract','vehicle','gives the keys','marks the contract active'];

  let cc=['Customer','clerk','vehicle','reservation'];
  // cc is for conceptual classes
  let nound=[]
  let ccd=[]
  let [nouns,setNouns]=useState([])
  let [ccs,setCcs]=useState([])
  function handleClck(e){
      for(let i=0;i<noun.length;i++){
          if(noun.includes(e.target.textContent)&&nound.indexOf(e.target.textContent)===-1&&nouns.indexOf(e.target.textContent)===-1)
          {   
              nound=[...nouns]
              nound.push(e.target.textContent);
              setNouns(nound);
              if(nound.includes('Customer')){
                setshownc(true);
                setmsg(false)
              }
               if(nound.includes('acquire a vehicle')){
                setshownv(true);
                setmsg(false);
              }
               if(nound.includes('signs the contract')){
                setshowncusn1(true);
                setmsg(false);
              }
              if(nound.includes('clerk')){
                setshowncl(true);
                setmsg(false);
              }
              if(nound.includes('locates the vehicle')){
                setshowncln(true);
                setmsg(false);
              }
          
              if(nound.includes('reservation')){
                setshownreserv(true);
                setmsg(false);
              }
              if(nound.includes('gives the keys')){
                setshownvb1(true);
                setmsg(false);
              }
              if(nound.includes('vehicle')){
                setshownvb(true);
                setmsg(false);
              }
              if(nound.includes('marks the contract active')){
                setshownvrd(true);
                setmsg(false);
              }
              if(nound.includes('vehicle type is not available')){
                setshowncusm(true);
                setmsg(false);
              }
             
              else{

              }
          }
       
      }
  }
  function handle(){
    setmsg(true)
  }
  function handleClckcc(e){
      console.log(e);
 
      for(let i=0;i<cc.length;i++){
       
          if(cc.includes(e.target.textContent)&&ccd.indexOf(e.target.textContent)===-1&&ccs.indexOf(e.target.textContent)===-1)
          {   
              ccd=[...ccs]
              ccd.push(e.target.textContent);
              setCcs(ccd);
            
              if(ccd.includes('Customer')){
                setshownc(true);
                setmsgg(false);
                setmsg(false);
              }

             
      }
      
    }
  }


function handleClckacq(){
  setmsgg(true)
}

function handleClcksc(){
  setmsgg(true)
}
function handleClcklv(){
  setmsgg(true)
}
function handleClckmc(){
  setmsgg(true)
}
function handleClckgi(){
  setmsgg(true)
}

function handleClckvt(){
  setmsgg(true)
}

 
    
    return (
      <>
    <Navbar />
    <div className='head'>
      <h2>Class Identification</h2>
    </div>
    <div style={{display:'flex',width:'80%',margin:'0 auto',justifyContent:'space-around'}}>
        <div>
        <div className='noun'>
      

      <div className='uc_ul'> 
       <div className='nounmsg'><b>Please select noun-phrases from the sentences given below.</b>
       </div> 
           <h4>
            <ul id='ul_1'>
              <li>1.   </li>
              <li id='li'onClick={handleClck} style={shownc?{color:'green'}:null}>Customer</li>
              <li id='li' onClick={handle}>comes</li>
              <li id='li' onClick={handle}>to</li>
              <li id='li' onClick={handle}>the</li>
              <li id='li' onClick={handle}>office</li>
              <li id='li' onClick={handle}>to</li>
              {/* <li id='li' onClick={handleClicknn}>acquire</li> */}
              <li id='li'onClick={handleClck}style={shownv?{color:'green'}:null}>acquire a vehicle</li>
              <li id='li'onClick={handle}>.</li>
          </ul>
          <ul id='ul_2'>
        <li>2.  </li> 
        <li id='li'onClick={handle}>The</li>
        <li id='li' onClick={handleClck} style={showncl?{color:'purple'}:null}>clerk</li>
        <li id='li' onClick={handleClck} style={showncln?{color:'purple'}:null}>locates the vehicle</li>
        <li id='li' onClick={handleClck} style={shownreserv?{color:'orange'}:null}>reservation</li>
        <li id='li' onClick={handle}>contract</li>
        <li id='li' onClick={handle}>by</li>
        <li id='li' onClick={handle}>means</li><br></br>
        <li id='li' onClick={handle}>of</li>
        <li id='li' onClick={handle}>the</li>
        <li id='li' onClick={handle}>reservation number</li>
        <li id='li' onClick={handle}>and/or</li>
        <li id='li' onClick={handle}>customer name</li>
        <li id='li'onClick={handle}>.</li> 
        </ul>

        <ul id='ul_6'>
        <li id='li'onClick={handle}>[</li>
        <li id='li' onClick={handle}>Exception:</li>
        <li id='li'onClick={handle}>Required </li>
        <li id='li' onClick={handleClck} style={showncusm?{color:'orange'}:null}>vehicle type is not available</li>
        <li id='li' onClick={handle}>due</li>
        <li id='li'onClick={handle}>to</li>
        <li id='li' onClick={handle}>late</li>
        <li id='li' onClick={handle}>arrivals</li>
        <li id='li' onClick={handle}>.]</li>
        </ul>
      <br/>
      <ul id='ul_3'>
        <li>3.   </li>
        <li id='li' onClick={handle}>The</li>
        <li id='li' onClick={handleClck} style={shownc?{color:'green'}:null}>Customer</li>
        <li id='li' onClick={handleClck} style={showncusn1?{color:'green'}:null}>signs the contract</li>
        <li id='li' onClick={handle}>and</li>
        <li id='li' onClick={handle}>the</li>
        <li id='li' onClick={handleClck}style={showncl?{color:'purple'}:null}>clerk</li>
        <li id='li' onClick={handleClck} style={shownvb1?{color:'rgb(52, 142, 238)'}:null} >gives the keys</li>
        <li id='li'onClick={handle}>of</li>
        <li id='li'onClick={handle}>the</li>
        <li id='li' onClick={handleClck} style={shownvb?{color:'rgb(52, 142, 238)'}:null} >vehicle</li>
        <li id='li' onClick={handle}>.</li>
      </ul>
      
      <br/>

      <ul id='ul_4'>
        <li>4.</li>
        <li id='li' onClick={handle}>The</li>
        <li id='li'  onClick={handleClck}style={showncl?{color:'purple'}:null}>clerk</li>
        <li id='li' onClick={handle}>then</li>
        <li id='li' onClick={handleClck} style={shownvrd?{color:'purple'}:null}>marks the contract active</li>
        <li id='li' onClick={handle}>by</li>
        <li id='li' onClick={handle}>entering</li>
        <li id='li' onClick={handle}>the</li>
        <li id='li'onClick={handle}>vehicle release date</li>
        </ul>
        <ul id='ul_7'>
        <li id='li' onClick={handle}>(today's</li>
        <li id='li' onClick={handle}>date)</li>
        <li id='li' onClick={handle}>onto</li>
        <li id='li' onClick={handle}>the</li>
        <li id='li' onClick={handle}>vehicle reservation contract</li>
        <li id='li'onClick={handle}>.</li>
      </ul>     
      <br/>
      <ul id='ul_5'>
       <li>5.</li>
        <li id='li' onClick={handle}>The</li>
        <li id='li' onClick={handle}>use case</li>
        <li id='li' onClick={handle}>use case</li>
        <li id='li' onClick={handle}>use case</li>
        <li id='li' onClick={handle}>terminates</li>
        <li id='li'onClick={handle}>at</li>
        <li id='li'onClick={handle}>this</li>
        <li id='li'onClick={handle}>point</li>
        <li id='li'onClick={handle}>.</li>
      </ul>
      </h4>
      <br/>
      {msg? <div className="msg"><Message type="info">
       <h3><b>Selected word is not a noun-phrase. Find the correct one.</b></h3>
            </Message></div>: null}

      {msgg? <div className="msg1"><Message type="info">
       <h3><b>Selected noun phrases not a conceptual classes.</b></h3>
            </Message></div>: null}      
        </div>
        
        <div className="boxes"><center> <u><h4>Identified Noun Phrase</h4></u></center>
        <div className='customer-div' style={{border:"1px solid black",width:"90%",margin:"1rem auto",minHeight:"2rem"}}>
           
            {nouns.map((nou)=>{if(nou=="Customer"){return(<li key={nou} onClick={handleClckcc}><h4>{nou}</h4></li>)}})}
            {nouns.map((nou)=>{if(nou=="acquire a vehicle"){return(<li key={nou} onClick={handleClckacq}><h4>{nou}</h4></li>)}})}
            {nouns.map((nou)=>{if(nou=="signs the contract"){return(<li key={nou} onClick={handleClcksc}><h4>{nou}</h4></li>)}})}
</div>
<div className='clerk-div' style={{border:"1px solid black",width:"90%",margin:"1rem auto",minHeight:"2rem"}}>
    {nouns.map((nou)=>{if(nou=="clerk"){return(<li key={nou} onClick={handleClckcc}><h4>{nou}</h4></li>)}})}
    {nouns.map((nou)=>{if(nou=="locates the vehicle"){return(<li key={nou} onClick={handleClcklv}><h4>{nou}</h4></li>)}})}
    {nouns.map((nou)=>{if(nou=="marks the contract active"){return(<li key={nou} onClick={handleClckmc}><h4>{nou}</h4></li>)}})}
</div>

<div className='vehicle' style={{border:"1px solid black",width:"90%",margin:"1rem auto",minHeight:"2rem"}}>

{nouns.map((nou)=>{if(nou=="vehicle"){return(<li key={nou} onClick={handleClckcc}><h4>{nou}</h4></li>)}})}
{nouns.map((nou)=>{if(nou=="gives the keys"){return(<li key={nou} onClick={handleClckgi}><h4>{nou}</h4></li>)}})}

</div>

<div className='reservation' style={{border:"1px solid black",width:"90%",margin:"1rem auto",minHeight:"2rem"}}>
{nouns.map((nou)=>{if(nou=="reservation"){return(<li key={nou} onClick={handleClckcc}><h4>{nou}</h4></li>)}})}
{nouns.map((nou)=>{if(nou=="vehicle type is not available"){return(<li key={nou} onClick={handleClckvt}><h4>{nou}</h4></li>)}})}

</div>          
        </div>
        <br/>
        <div className='classbox'><center><u><h4>Noun as Conceptual Classes</h4></u></center>
            <div className='customer-div2' style={{width:"90%",margin:"1rem auto",minHeight:"2rem"}}>
            {ccs.map((cc)=>{if(cc=="Customer"){return(<li key={cc}><h4>{cc}</h4></li>)}})}

            </div>
            <div className='clerk-div2' style={{width:"90%",margin:"1rem auto",minHeight:"2rem"}}>
            {ccs.map((cc)=>{if(cc=="clerk"){return(<li key={cc}><h4>{cc}</h4></li>)}})}
             </div>

             <div className='vehicle-div2' style={{width:"90%",margin:"1rem auto",minHeight:"2rem"}}>
             {ccs.map((cc)=>{if(cc=="vehicle"){return(<li key={cc}><h4>{cc}</h4></li>)}})}
    </div>

    <div className='reservation-div2' style={{width:"90%",margin:"1rem auto",minHeight:"2rem"}}>
    {ccs.map((cc)=>{if(cc==="reservation"){return(<li key={cc}><h4>{cc}</h4></li>)}})}
         </div>

        </div>
        
    </div>
    </div>
    </div>
    </>
  )
}
export default Component1