import React, { useState } from "react";
import './style.css';
import List, { ItemDragging } from "devextreme-react/list";
import shuffle from "lodash.shuffle";
import 'devextreme/dist/css/dx.light.css';
import 'devextreme/dist/css/dx.common.css';
import { usecaseTasks, shuffledTasks } from "./data.js";
import { Message } from 'rsuite';
import { set } from "lodash";
import {Link} from 'react-router-dom'
import { CommonSeriesSettingsHoverStyle } from "devextreme-react/chart";



function Jkl() {
  const [usecaseTasksState, setusecaseTasks] = useState(usecaseTasks);//use_case task from data.js
  const [shuffledTasksState, setshuffledTasks] = useState(shuffle(shuffledTasks));
  const [currentTaskState,setcurrentTasks]=useState(0);
  const [currentTaskItemIndex,setCurrentTaskItemIndex]=useState(-1);
  const [taskState,setTaskState]=useState(false);
  const [background,setbackground]=useState(false);
  const [showmsg,setShowmsg]=useState(false);
  const [showans,setShowans]=useState(0);
  const array=[""]
  const array1=[""]
  const [counter, setCounter] = useState(0);
  const incrementCounter = () => setCounter(counter + 1);
  const [dcount,setDcount]=useState(0)
  const userSelection=[];

  let count=0; 
  
   const onDragStart = (e) => {   

    console.log(e);
    console.log('hello');
    console.log(e.toData);

    const tasks = e.toData === "shuffledTasks" ? usecaseTasksState : shuffledTasksState;
//e.toData undefined
    console.log(tasks);
    //tasks- Whole data that we stored in shuffledTasks
    e.itemData = tasks[e.fromIndex]; 
// checking if tasks[2]=e.itemData
    setcurrentTasks(e.itemData);
// state changes from 0 to 2
    setCurrentTaskItemIndex(e.fromIndex);
// state changes from -1 to 0
    setTaskState(false);

    };

  const onAdd = (e) => {
    console.log(e); //getting dropped element
    console.log("Hello worldd");
    console.log(e.toData);
    const tasks = ( e.toData === "shuffledTasks" ? shuffledTasksState : usecaseTasksState );    
  //  console.dir(currentTaskState);
    const setTasks = (e.toData === "shuffledTasks" ? setshuffledTasks : setusecaseTasks);
    incrementCounter();
    array[counter]=[currentTaskState.id ]
    array1[counter]=[usecaseTasksState[e.toIndex].id ]
    console.log(array);

    if(usecaseTasksState[e.toIndex].text.length == 0){
      setTasks([...tasks.slice(0, e.toIndex), e.itemData,...tasks.slice(e.toIndex+1)]);
      setTaskState(true);
      userSelection.push(e.itemData.id)
      console.log('hello world') ;  
      console.log(tasks);
  }
  };  
 


  const onRemove = (e) => {
    const tasks = ( e.fromData === "shuffledTasks" ? shuffledTasksState : usecaseTasksState );    
    const setTasks = (e.fromData === "shuffledTasks" ? setshuffledTasks : setusecaseTasks);
    
    if(taskState ===true) 
    
   {setTasks([...tasks.slice(0, currentTaskItemIndex), ...tasks.slice(currentTaskItemIndex + 1) ]); } 
  };
  
  const onReorder = (e) => {
    onRemove(e);
    onAdd(e);
    
  };

  function checkbtnn(){
    for(let i=0;i<5;i++){
      if(shuffledTasks[i] == usecaseTasksState[i])
      {
      setShowmsg(true);
      console.log(showmsg);
      setShowans(showans+1); 
    }
    else
    {
      console.log(shuffledTasks[i]);
      setShowmsg(false);
      console.log(showmsg);
      setDcount(dcount+1);
    }
  }
    
}

// function checkbtn(){
//   for(var k=0;k<counter;k++){

//     if(array[k]=== array1[k] ){
     
//       setTaskState(true);
//       setShowmsg(false);
     
//     }
//     else{
//       setShowmsg(true);
//       setShowans(false);
//     }
//   }
// }


  return (
   <>
    <div className="widget-container">
       
      <div className="ins">
       <div className="insdiv"><center>Car Rental Application</center></div>
       <ul className="data">
       <li><b>Use-Case Name</b>:<br/>Release a vehicle (to a customer).</li>
       <li><b>Actors</b>: <ul><li>Front Desk Clerk</li><li>Customer</li></ul></li>
       <li><b>Preconditions</b>:<br/> Vehicle has been assigned to the customer.</li>
       </ul>
      
      </div>
      
      <div className="sim">
      <div className="insdiv2"><b>Correct Order</b></div>

      
      <List items={usecaseTasksState} keyExpr="id" repaintChangesOnly={true}
    className={background?"background-red":'background-white'}
      >
         
        <ItemDragging
          allowReordering={true}
          group="tasks"
          data="usecaseTasksState"
          onDragStart={onDragStart}
          onAdd={onAdd}
          onRemove={onRemove}
        />
      </List>
      
      
      {/* <div className="not"><h3>Notifications:</h3></div>
       {showmsg? <div className="msgdiv">
        {showmsg? <div className="msg"><Message type="info">
       <h3>You have selected the wrong answer. Place the right answer.</h3>
            </Message></div>: null}
           
      </div>:null}
      {showans? <div className="msgdiv1">
      {showans? <div className="answer"> <Message type="info">
       <h3>Your answer is in right position. Go Ahead</h3>
            </Message> </div>: null}
            </div>:null} */}
            {/* <div className="not"><h3>Notifications:</h3></div> */}
            
       {dcount>0? <div className="msgdiv">
        {dcount>0? <div className="msg"><Message type="info">
       <h3>You have selected the wrong answer. Place the right answer.</h3>
            </Message></div>: null}
           
      </div>:null}
      {showans==5? <div className="msgdiv1">
      {showans==5? <div className="answer"> <Message type="info">
       <h3>Your answer is in right position. Go Ahead</h3>
            </Message> </div>: null}
            </div>:null} 
               <div className="btn2" onClick={checkbtnn}><button>Evaluate</button></div>
      </div> 
      <div className="sim2">
      <div className="insdiv3"><b>Jumbled Instructions</b></div>
      
      <List items={shuffledTasksState} keyExpr="id" repaintChangesOnly={true}
      > 
        <ItemDragging
       
          allowReordering={false}
          group="tasks"
          data="shuffledTasks"
          onDragStart={onDragStart}
          onAdd={onAdd}
          onReoder={onReorder}
          onRemove={onRemove}
      
        />
      
      </List>
      
      <div className="btn1"><Link to='/noun'><button>Identify the<br/> Noun Phrase</button></Link></div>
   
    </div>
    </div>
    
    </>
  );
}

export default Jkl;