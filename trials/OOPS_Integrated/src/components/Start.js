import React from 'react';
import './q.css'

const Start = ({ startQuiz, showStart }) => {
    return (
        <section className='start' style={{ display: `${showStart ? 'block' : 'none'}` }}>
            <div className='container'>
                <div className="justify-content-center">
                    <div className="col-lg-8" justify-content-center >
                        <h1 className='fw-bold mb-4'>Test your Knowledge here.</h1>
                        <button onClick={startQuiz} className="startbtn">Start Quiz</button>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Start;