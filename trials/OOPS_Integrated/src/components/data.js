import './style.css';
export const shuffledTasks = [{ id: 1, text: 'A customer comes to the office to acquire a vehicle.' },
  { id: 2, text: 'The clerk locates the vehicle reservation contract by means of the reservation number and/or customer name.' },
  { id: 3, text: 'The customer signs the contract and the clerk gives the keys to the vehicle.' },
  { id: 4, text: 'The clerk then marks the contract active by entering the vehicle release date onto the vehicle reservation contract.' },
  { id: 5, text: 'The use case terminates at this point.' }
];

export const usecaseTasks = 
  [{ id:1, text: ''},
  { id:2, text: '' },
  { id:3, text: '' },
  { id:4, text: '' },
  { id:5, text: '' }];

export const currentTaskState = 0;