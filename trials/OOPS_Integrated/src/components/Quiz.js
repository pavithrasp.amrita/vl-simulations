import retryEnhancer, { RETRY_EVENT, useRetry } from '@rpldy/retry-hooks';
import React, { useCallback } from 'react';
import './q.css'

const Quiz = ({ showQuiz, question, quizs, checkAnswer, correctAnswer, selectedAnswer, questionIndex, nextQuestion, prevQuestion, showTheResult,startOver}) => {

    return (
        <section className="bg" style={{ display: `${showQuiz ? 'block' : 'none'}` }}>
            <div className="container">
                <div className="cardp">
                    <div>
                        <div className="cardp-4">
                            <div className="q">
                                 <h6 className='qno'>Q{quizs.indexOf(question) + 1} . </h6>
                                  <h5 className='mb-2 fs-normal lh-base'>{question?.question}</h5>
                                
                            </div>
                            <div>
                                {
                                    question?.options?.map((item, index) => <button
                                        key={index}
                                        className={`option w-100 text-start btn text-dark py-2 px-3 mt-3 rounded btn-info ${correctAnswer === item && 'bg-success'}`}
                                        onClick={(event) => checkAnswer(event, item)}
                                    >
                                        
                                        {item}
                                    </button>)
                                }
                                
                            </div>
                            
                            {
                                (questionIndex + 1) !== quizs.length ?
                                <div><button className='btn py-2 w-100 mt-3 bg-dark text-white fw-bold' onClick={nextQuestion} disabled={!selectedAnswer}>Next Question</button>
                                </div>
                                
                                   
                                    :
                                    <button className='btn py-2 w-100 mt-3 bg-dark text-white fw-bold' onClick={showTheResult} disabled={!selectedAnswer}>Show Result</button>
                            }
                            { (questionIndex + 1) === 1 ?
                                        <button className='btn py-2 w-100 mt-3 bg-dark text-white fw-bold' onClick={prevQuestion} disabled>Prev Question</button>
                                        :
                                        <button className='btn py-2 w-100 mt-3 bg-dark text-white fw-bold' onClick={prevQuestion}>Prev Question</button>
                                        
                            }
                            <div>
                            <button className='btn py-2 w-100 mt-3 bg-dark text-white fw-bold' onClick={startOver}>Reset</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Quiz;