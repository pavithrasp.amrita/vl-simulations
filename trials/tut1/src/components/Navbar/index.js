import React from 'react'
import {
    NavLink,Nav, Bars} from './components/Navbar/Navbar.js';


const Navbar = () => {
  return (
  <>
    <Nav>
        <NavLink to="/">
            <h1>Hello</h1>
        </NavLink>
    <Bars/>
    <NavLink to="/introduction" activeStyle>
        Introduction
    </NavLink>
    <NavLink to="/Theory" activeStyle>
        Theory
    </NavLink>
    <NavLink to="/Simulations" activeStyle>
        Simulations
    </NavLink>
    <NavLink to="/Quiz" activeStyle>
        Quiz
    </NavLink></Nav>
  </>
  );
}

export default Navbar;