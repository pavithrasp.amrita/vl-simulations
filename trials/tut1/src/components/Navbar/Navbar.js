import styled from "styled-components"
import {NavLink as Link} from 'react-router-dom'
import {FaBars} from 'react-icons/fa'


export const NavLink = styled(Link)
export const Navbar=styled(FaBars)