import React, { useState, useCallback } from "react";

import List, { ItemDragging } from "devextreme-react/list";
import shuffle from "lodash.shuffle";

import { usecaseTasks, shuffledTasks } from "./data.js";
import {Navbar} from "./components/Navbar/Navbar.js";
import { BrowserRouter as Router } from "react-router-dom";

function App() {
  const [usecaseTasksState, setusecaseTasks] = useState(usecaseTasks);
  const [shuffledTasksState, setshuffledTasks] = useState(shuffle(shuffledTasks));
  const [currentTaskState,setcurrentTasks]=useState(0);
  const [currentTaskItemIndex,setCurrentTaskItemIndex]=useState(-1);
  const [taskState,setTaskState]=useState(false);

  const onDragStart = (e) => {   
    const tasks = e.toData === "shuffledTasks" ? usecaseTasksState : shuffledTasksState;
    e.itemData = tasks[e.fromIndex];    
    setcurrentTasks(e.itemData);
    setCurrentTaskItemIndex(e.fromIndex);
    setTaskState(false);
    };

  const onAdd = (e) => {

    const tasks = ( e.toData === "shuffledTasks" ? shuffledTasksState : usecaseTasksState );    
    const setTasks = (e.toData === "shuffledTasks" ? setshuffledTasks : setusecaseTasks);

    if(currentTaskState.id == usecaseTasksState[e.toIndex].id){
      setTasks([...tasks.slice(0, e.toIndex), e.itemData,...tasks.slice(e.toIndex+1)]);
      setTaskState(true);
    }    
  };

  const onRemove = (e) => {
   
    const tasks = ( e.fromData === "shuffledTasks" ? shuffledTasksState : usecaseTasksState );    
    const setTasks = (e.fromData === "shuffledTasks" ? setshuffledTasks : setusecaseTasks);
    if(taskState == true)
      setTasks([...tasks.slice(0, currentTaskItemIndex), ...tasks.slice(currentTaskItemIndex + 1)]);  
    
  };

  const onReorder = (e) => {
    onRemove(e);
    onAdd(e);
  };
  return (
    
    <div className="widget-container">
      <Router>
<Navbar/>
</Router>
      
      <List items={usecaseTasksState} keyExpr="id" repaintChangesOnly={true}>
        <ItemDragging
          allowReordering={true}
          group="tasks"
          data="usecaseTasksState"
          onDragStart={onDragStart}
          onAdd={onAdd}
          onRemove={onRemove}
        />
      </List>
      <List items={shuffledTasksState} keyExpr="id" repaintChangesOnly={true}>
        <ItemDragging
          allowReordering={false}
          group="tasks"
          data="shuffledTasks"
          onDragStart={onDragStart}
          onAdd={onAdd}
          onReoder={onReorder}
          onRemove={onRemove}
        />
      </List>
    
    
    </div>
  )
}

export default App;
