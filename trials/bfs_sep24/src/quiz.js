import React, { useState } from 'react';

export default function Quiz() {
	const questions = [
		{
			questionText: 'Which data structure is used in the breadth first search of a graph to store nodes?',
			answerOptions: [
				{ answerText: 'Array', isCorrect: false },
				{ answerText: 'Tree', isCorrect: false },
				{ answerText: 'Queue', isCorrect: true },
				{ answerText: 'Heap', isCorrect: false },
			],
		},
		{
            questionText: 'Time Complexity of Breadth First Search is? (V – number of vertices, E – number of edges)',
            answerOptions: [
                { answerText: 'O(V)', isCorrect: false },
                { answerText: 'O(V + E)', isCorrect: true },
                { answerText: 'O(E)', isCorrect: false },
                { answerText: 'O(V*E)', isCorrect: false },
            ],
        },
        {
            questionText: 'The Data structure used in standard implementation of Breadth First Search is?',
            answerOptions: [
                { answerText: 'Queue', isCorrect: true },
                { answerText: 'Stack', isCorrect: false },
                { answerText: 'Linked List', isCorrect: false },
                { answerText: 'Tree', isCorrect: false },
            ],
        },
        {
            questionText: 'The Breadth First Search traversal of a graph will result into?',
            answerOptions: [
                { answerText: 'Graph with back edges', isCorrect: false },
                { answerText: 'Linked List', isCorrect: false },
                { answerText: 'Arrays', isCorrect: false },
                { answerText: 'Tree', isCorrect: true },
            ],
        },
        {
            questionText: 'A person wants to visit some places. He starts from a vertex and then wants to visit every place connected to this vertex and so on. What algorithm he should use?',
            answerOptions: [
                { answerText: 'Depth First Search', isCorrect: false },
                { answerText: 'Trim’s algorithm', isCorrect: false },
                { answerText: 'Kruskal’s algorithm', isCorrect: false },
                { answerText: 'Breadth First Search', isCorrect: true },
            ],
        },
        {
            questionText: 'Which of the following is not an application of Breadth First Search?',
            answerOptions: [
                { answerText: 'Finding shortest path between two nodes', isCorrect: false },
                { answerText: ' Path Finding', isCorrect: true },
				{ answerText: ' Finding bipartiteness of a graph', isCorrect: false},
				{ answerText: ' GPS navigation system', isCorrect: false },
            ],
        },
        {
            questionText: 'When the Breadth First Search of a graph is unique?',
            answerOptions: [
                { answerText: 'When the graph is a Binary Tree', isCorrect: false },
                { answerText: 'When the graph is a n-ary Tree', isCorrect: false },
                { answerText: ' When the graph is a Ternary Tree', isCorrect: false },
                { answerText: 'When the graph is a Linked List', isCorrect: true },
            ],
        },
        {
            questionText: 'Regarding implementation of Breadth First Search using queues, what is the maximum distance between two nodes present in the queue? (considering each edge length 1)',
            answerOptions: [
                { answerText: 'Can be anything', isCorrect: false },
                { answerText: '0', isCorrect: false },
                { answerText: ' Insufficient Information', isCorrect: false },
                { answerText: 'At most 1', isCorrect: true },
            ],
        },
        {
            questionText: ' In BFS, how many times a node is visited?',
            answerOptions: [
                { answerText: ' Once', isCorrect: false },
                { answerText: 'Equivalent to number of indegree of the node', isCorrect: true},
				{ answerText: 'Twice', isCorrect: false },
				{ answerText: 'Thrise', isCorrect: false },
            ],
        },
        {
            questionText: ' Depth First Search is equivalent to which of the traversal in the Binary Trees?',
            answerOptions: [
                { answerText: 'Post-order Traversal', isCorrect: false },
                { answerText: 'Level-order Traversal', isCorrect: false },
                { answerText: 'In-order Traversal', isCorrect: false },
                { answerText: 'Pre-order Traversal', isCorrect: true },
            ],
        },
    ];

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);
    const [showborder,setShowborder]=useState();
    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setScore(score + 1);
            setShowborder(false);
            // setCurrentQuestion(nextQuestion);
        }
        else{
            setShowborder(true);
            setCurrentQuestion(nextQuestion);
        }

        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < questions.length)
            {
                setCurrentQuestion(nextQuestion);
            }
           
           
           
         else {
            setShowScore(true);
            setShowborder(false)
        }
    };
    return (
        <div className='app'>
            {showScore ? (
                <div className='score-section'>
                    You scored { score } out of { questions.length }
                </div>
            ) : (
                <>
                   
                    <div className='question-section'>

                        <div className='qhead'></div>
                        <div className='question-count'>
                            <span>Question {currentQuestion + 1}</span>/{questions.length}
                        </div>
                        <div className='question-text'>{questions[currentQuestion].questionText}
						{}
						</div>
                        {showborder?<div className='txt'><h3>Your answer is wrong</h3></div>:null}
                    </div>
                    <div className={'answer-section'} >
                        {questions[currentQuestion].answerOptions.map((answerOption) => (
                            <button onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
                        ))}
                    </div>
                   
                   
                </>
            )}
        </div>
    );
}