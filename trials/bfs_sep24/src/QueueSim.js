import React from "react";

const QueueSim = ({queue}) => {
    return (
        <div className="pr-5 absolute right-0 top-0 mt-32 overflow-y-auto" style={{maxHeight: '60vh'}}>
            {queue.length > 0 && (
                <div className="pb-3">
                    <span className="text-gray-400 text-center">Traversing</span><br/>
                    <span className="w-full block text-gray-400 text-center">path</span>
                </div>
            )}
            <div className="flex flex-col justify-center items-center">
                {queue.map((elm, i) => {
                    return (
                        <div className="border-2 w-min border-gray-600 p-4 text-white" key={i}>{elm}</div>
                    )}
                )}
            </div>
        </div>
    )
}

export default QueueSim;