import React from 'react';
import Initializer from "./Initializer";
import GraphSim from "./GraphSim";
import ControlPanel from './Initializer';
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./Navigation/Navbar.js";
import { BrowserRouter } from 'react-router-dom';
import { Routes } from 'react-router-dom';
import { Route } from 'react-router-dom';
import Quiz from './quiz';

const App = () => {

  const [graphData, setGraphData] = React.useState({startingNode: 1, noOfNodes: 5, randomize: true, flag: 1});

  return (
<>
<BrowserRouter>
  <Routes>
    <Route path='/' element={<div className="flex justify-between bg-gray-800/90  items-center" style={{width:'100vw', height:'100vh'}}>
        <div className='flex flex-col h-screen overflow-hidden'>
          
        </div>
        <Initializer graphData={graphData} setGraphData={setGraphData}/>
        <GraphSim graphData={graphData} setGraphData={setGraphData} />      
      </div>}></Route>
<Route path='quiz' element={<Quiz/>}></Route>
  </Routes>
</BrowserRouter>
</>
    
/*       <div className="flex justify-between bg-gray-800/90  items-center" style={{width:'100vw', height:'100vh'}}>
        <div className='flex flex-col h-screen overflow-hidden'>
          
        </div>
        <Initializer graphData={graphData} setGraphData={setGraphData}/>
        <GraphSim graphData={graphData} setGraphData={setGraphData} />      
      </div> */




  )
}

export default App
