import React, { useState } from 'react';

export default function dfsquiz() {
	const questions = [
		{
			questionText: 'Time Complexity of DFS is? (V – number of vertices, E – number of edges) ?',
			answerOptions: [
				{ answerText: ' O(V + E)', isCorrect: true },
				{ answerText: ' O(V)', isCorrect: false },
				{ answerText: 'O(E)', isCorrect: false },
				{ answerText: 'O(V*E)', isCorrect: false },
			],
		},
		{
            questionText: 'The Depth First Search traversal of a graph will result into?',
            answerOptions: [
                { answerText: 'Linked List', isCorrect: false },
                { answerText: 'Tree', isCorrect: true },
                { answerText: ' Graph with back edges', isCorrect: false },
                { answerText: 'Array', isCorrect: false },
            ],
        },
        {
            questionText: ' Which of the following is not an application of Depth First Search?',
            answerOptions: [
                { answerText: 'For generating topological sort of a graph', isCorrect: false },
                { answerText: 'For generating Strongly Connected Components of a directed graph', isCorrect: false },
                { answerText: 'Detecting cycles in the graph', isCorrect: false },
                { answerText: 'Peer to Peer Networks', isCorrect: true },
            ],
        },
        {
            questionText: 'When the Depth First Search of a graph is unique?',
            answerOptions: [
                { answerText: ' When the graph is a Binary Tree', isCorrect: false },
                { answerText: 'When the graph is a Linked List', isCorrect: true },
                { answerText: 'When the graph is a n-ary Tree', isCorrect: false },
                { answerText: 'When the graph is a ternary Tree', isCorrect: false },
            ],
        },
        {
            questionText: 'In Depth First Search, how many times a node is visited?',
            answerOptions: [
                { answerText: ' Once', isCorrect: false },
                { answerText: 'Twice', isCorrect: false },
                { answerText: 'Equivalent to number of indegree of the node', isCorrect: true },
                { answerText: 'Thrice', isCorrect: false },
            ],
        },
        {
            questionText: 'Depth First Search is equivalent to which of the traversal in the Binary Trees?',
            answerOptions: [
                { answerText: ' Post-order Traversal', isCorrect: false },
                { answerText: ' Pre-order Traversal', isCorrect: true },
				{ answerText: ' Level-order Traversal', isCorrect: false},
				{ answerText: ' In-order Traversal', isCorrect: false },
            ],
        },
        {
            questionText: 'The Depth First Search traversal of a graph will result into?',
            answerOptions: [
                { answerText: 'None of the mentioned', isCorrect: false },
                { answerText: 'Graph with back edges', isCorrect: false },
                { answerText: ' Linked list', isCorrect: false },
                { answerText: 'Tree', isCorrect: true },
            ],
        },
        {
            questionText: 'When the Depth First Search of a graph is unique?',
            answerOptions: [
                { answerText: 'When the graph is a Binary Tree', isCorrect: false },
                { answerText: 'When the graph is a n-ary Tree', isCorrect: false },
                { answerText: 'When the graph is a Linked List', isCorrect: true },
                { answerText: 'None of the mentioned', isCorrect: false },
            ],
        },
        {
            questionText: ' In Depth First Search, how many times a node is visited?',
            answerOptions: [
                { answerText: ' Once', isCorrect: false },
                { answerText: 'Equivalent to number of indegree of the node', isCorrect: true},
				{ answerText: 'Twice', isCorrect: false },
				{ answerText: 'Thrice', isCorrect: false },
            ],
        },
        {
            questionText: ' Depth First Search is equivalent to which of the traversal in the Binary Trees?',
            answerOptions: [
                { answerText: 'Post-order Traversal', isCorrect: false },
                { answerText: 'Level-order Traversal', isCorrect: false },
                { answerText: 'In-order Traversal', isCorrect: false },
                { answerText: 'Pre-order Traversal', isCorrect: true },
            ],
        },
    ];

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);
    const [showborder,setShowborder]=useState();
    const handleAnswerOptionClick = (isCorrect) => {
        if (isCorrect) {
            setScore(score + 1);
            setShowborder(false);
            // setCurrentQuestion(nextQuestion);
        }
        else{
            setShowborder(true);
            setCurrentQuestion(nextQuestion);
        }

        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < questions.length)
            {
                setCurrentQuestion(nextQuestion);
            }
           
           
           
         else {
            setShowScore(true);
            setShowborder(false)
        }
    };
    return (
        <div className='app'>
            {showScore ? (
                <div className='score-section'>
                    You scored { score } out of { questions.length }
                </div>
            ) : (
                <>
                   
                    <div className='question-section'>

                        <div className='qhead'></div>
                        <div className='question-count'>
                            <span>Question {currentQuestion + 1}</span>/{questions.length}
                        </div>
                        <div className='question-text'>{questions[currentQuestion].questionText}
						{}
						</div>
                        {showborder?<div className='txt'><h3>Your answer is wrong</h3></div>:null}
                    </div>
                    <div className={'answer-section'} >
                        {questions[currentQuestion].answerOptions.map((answerOption) => (
                            <button onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
                        ))}
                    </div>
                   
                   
                </>
            )}
        </div>
    );
}