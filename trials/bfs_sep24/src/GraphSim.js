import React, {lazy, useEffect, useRef} from "react";
import cytoscape from "cytoscape";
import {option} from "./option";
import QueueSim from "./QueueSim";
import Quiz from "./quiz";

const randomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const generateEdges = (graphData) => {
    option.elements.edges = [];

    for(let i = 0; i < graphData.noOfNodes+randomNumber(0,4); i++) {
        const a = randomNumber(1, graphData.noOfNodes);
        const b = randomNumber(1, graphData.noOfNodes);
        if (a !== b && !option.elements.edges.some(edge => {
            return edge.data.source === a && edge.data.target === b
        }))
        {
            option.elements.edges = [
                ...option.elements.edges,
                {
                    data: {
                        id: `${a}${b}`, weight: randomNumber(0, graphData.noOfNodes+4), source: `${a}`, target: `${b}`
                    }
                }
            ]
        }
    }
}

const GraphSim = ({graphData, setGraphData}) => {
    const graphRef = useRef(null)
    const [edge, setEdge] = React.useState([])
    const [prevGraphData, setPrevGraphData] = React.useState({...graphData, noOfNodes: 3});
    const [queue, setQueue] = React.useState([]);
    const [quiz,showQuiz] = React.useState(false);
    const drawGraph = () => {
        option.container = graphRef.current;
        const cy = cytoscape(option);
        console.log(cy.$("#2").renderedPosition())
        cy.on('tap', 'node', function(evt) {
            cy.$(`#${evt.target.id()}`).toggleClass('selected');
            setEdge(prev =>[...prev, evt.target.id()]);
        })

        if (graphData.flag === 3) {
            const bfs = cy.elements().bfs(`#${graphData.startingNode}`, () => {}, true);
            let i = 0;

            let queued = bfs.path.filter(path => path.group() === 'nodes').map(node => node.id())

            const highlightNextElm = () => {
                if (i < bfs.path.length) {
                    console.log(parseInt(bfs.path.$id));
                    console.log("helo");
                    bfs.path[i].addClass('highlighted');
                    i++;

                    setTimeout(() => {
                        setQueue(queued.slice(0, i));
                        
                        
                        // bfs.path[i-1].source()?.id()?.removeClass('active');
                        highlightNextElm()
                    }, 500);

                    console.log(bfs.path._private.map);
                    let k=bfs.path._private.map.Entries;
                    console.log(k);
                    console.log(bfs.path);
                }
                // setQueue([])
            }

            highlightNextElm()
            // setGraphData({...graphData, flag: 1})
        }
        setQueue([])
    }

    const generateNodes = () => {
        option.elements.nodes = [{ data: { id: `1` }}];

        if (graphData.noOfNodes > 1 && option.elements.nodes.length < graphData.noOfNodes)
            for(let i = 2; i <= graphData.noOfNodes; i++)
                option.elements.nodes = [...option.elements.nodes, { data: { id: `${i}` }}]

        setGraphData({...graphData, flag: 1})
    }

    useEffect(() => {
        if (prevGraphData.noOfNodes !== graphData.noOfNodes)
            generateNodes();

        if (graphData.flag !== 3 && graphData.flag !== 5 && !graphData.randomize)
            option.elements.edges = [];

        if (graphData.randomize && prevGraphData.noOfNodes !== graphData.noOfNodes)
            generateEdges(graphData);

        drawGraph()

        setPrevGraphData(graphData)

    }, [graphData])

    useEffect(() => {
        if (edge.length === 2){option.elements.edges = [...option.elements.edges,
                {
                    data: {
                        id: `${edge[0]}${edge[1]}`,
                        weight: randomNumber(0, graphData.noOfNodes+4),
                        source: `${edge[0]}`,
                        target: `${edge[1]}`
                    },
                 
                }]
            drawGraph();
            setEdge([])
            setGraphData({...graphData, flag: 5})
        }
    },[edge])


    return(
        <>  
            
            <div ref={graphRef} style={{width: '80%', height: '100vh'}} className="p-16 pr-32"/>
            <QueueSim queue={queue}/>
            {quiz?<Quiz />:null}
            
            
        </>
    )
}

export default GraphSim;