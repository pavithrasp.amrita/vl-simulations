
import React, { useCallback, useState } from 'react';
import './index.css';
import "./styles.css";
// import {Line} from 'react-lineto';
import { useRef } from 'react';
import  Xarrow from "react-xarrows";
// import useXarrow from 'react-xarrows';

function App() {
  const pos=useRef()
  const pos1=useRef()
  const pos2=useRef()
  const pos3=useRef()
  const pos4=useRef()
  const pos6=useRef()
  const pos7=useRef()
  const box1Ref = useRef(null);
const box2Ref = useRef(null);
const box3Ref= useRef(null);
  const boxStyle = {border: "grey solid 2px", borderRadius: "10px", padding: "2px"};
  function handleMove(){
    const drag=(e)=>{
      pos.current.style.top=`${e.pageY}px`;
      pos.current.style.left=`${e.pageX}px`;

      if(pos.current.style.top <='30px' && pos.current.style.left <='50px'){
        pos.current.style.border="2px solid green"
      }
      else{
        pos.current.style.border="2px solid red"
      }

    }
    pos.current.addEventListener('mousedown',()=>{
      window.addEventListener('mousemove',drag)

    })
    pos.current.addEventListener("mouseup",()=>{
      window.removeEventListener('mousemove',drag)
    })
  }

  function handleMove2(){
    const drag=(e)=>{
      pos2.current.style.top=`${e.pageY}px`;
      pos2.current.style.left=`${e.pageX}px`;

      if(pos2.current.style.top <='20px' && pos.current.style.left <='50px'){
        pos2.current.style.border="2px solid green"
      }
      else{
        pos2.current.style.border="2px solid red"
      }

    }
    pos2.current.addEventListener('mousedown',()=>{
      window.addEventListener('mousemove',drag)

    })
    pos2.current.addEventListener("mouseup",()=>{
      window.removeEventListener('mousemove',drag)
    })
  }
  function handleMove3(){
    const drag=(e)=>{
      pos3.current.style.top=`${e.pageY}px`;
      pos3.current.style.left=`${e.pageX}px`;

      if(pos3.current.style.top <='30px' && pos.current.style.left <='50px'){
        pos3.current.style.border="2px solid green"
      }
      else{
        pos3.current.style.border="2px solid red"
      }

    }
    pos3.current.addEventListener('mousedown',()=>{
      window.addEventListener('mousemove',drag)

    })
    pos3.current.addEventListener("mouseup",()=>{
      window.removeEventListener('mousemove',drag)
    })
  }

  function handleMove4(){
    const drag=(e)=>{
      pos4.current.style.top=`${e.pageY}px`;
      pos4.current.style.left=`${e.pageX}px`;

      if(pos4.current.style.top <='30px' && pos.current.style.left <='50px'){
        pos4.current.style.border="2px solid green"
      }
      else{
        pos4.current.style.border="2px solid red"
      }

    }
    pos4.current.addEventListener('mousedown',()=>{
      window.addEventListener('mousemove',drag)

    })
    pos4.current.addEventListener("mouseup",()=>{
      window.removeEventListener('mousemove',drag)
    })
  }
  function handleMove1(){
    const drag=(e)=>{
      pos1.current.style.top=`${e.pageY}px`;
      pos1.current.style.left=`${e.pageX}px`;

      if(pos1.current.style.top <='30px' && pos.current.style.left <='50px'){
        pos1.current.style.border="2px solid green"
      }
      else{
        pos1.current.style.border="2px solid red"
      }

    }
    pos1.current.addEventListener('mousedown',()=>{
      window.addEventListener('mousemove',drag)

    })
    pos1.current.addEventListener("mouseup",()=>{
      window.removeEventListener('mousemove',drag)
    })
  }

  function handleMove6(){
    const drag=(e)=>{
      pos6.current.style.top=`${e.pageY}px`;
      pos6.current.style.left=`${e.pageX}px`;

      if(pos6.current.style.top <='30px' && pos.current.style.left <='50px'){
        pos6.current.style.border="2px solid green"
      }
      else{
        pos6.current.style.border="2px solid red"
      }

    }
    pos6.current.addEventListener('mousedown',()=>{
      window.addEventListener('mousemove',drag)

    })
    pos6.current.addEventListener("mouseup",()=>{
      window.removeEventListener('mousemove',drag)
    })
  }

  function handleMove7(){
    const drag=(e)=>{
      pos7.current.style.top=`${e.pageY}px`;
      pos7.current.style.left=`${e.pageX}px`;

      if(pos7.current.style.top <='30px' && pos.current.style.left <='50px'){
        pos7.current.style.border="2px solid green"
      }
      else{
        pos7.current.style.border="2px solid red"
      }

    }
    pos7.current.addEventListener('mousedown',()=>{
      window.addEventListener('mousemove',drag)

    })
    pos7.current.addEventListener("mouseup",()=>{
      window.removeEventListener('mousemove',drag)
    })
  }


  return (
    <div>Domain Object Model
    <div className='qwe'>
      <div className='df'> 
     
      <h3> Relations</h3>
    
        <div style={{display: "inline-block",  width: "10%", paddingBottam:"40%",path:"grid"}}>
            <div className="first"ref={box1Ref} style={boxStyle}>customer</div>
            <div  className="second"id="second" style={boxStyle}>clerk</div>

            <Xarrow
           
                start={box1Ref} //can be react ref
                end="second" 
                path="grid"
               
            />
             
             {/* <Line from="first" to="second" /> */}
             <div className="first"ref={box1Ref} style={boxStyle}>customer</div>
            <div  className="third"id="third" ref={box2Ref}  style={boxStyle}>reservation</div>
            <Xarrow
           
                start={box1Ref} //can be react ref
                end="third" //or an id
            />


<div className="first"ref={box1Ref} style={boxStyle} >customer</div>
<div className="four"ref={box2Ref} style={boxStyle}>vehicle</div>
<Xarrow
           
           start={box1Ref} //can be react ref
           end="four"
           path="straight" //or an id
       />
 <div  className="second"id="second" ref={box3Ref}  style={boxStyle}>clerk</div>
 <div  className="four"id="four" style={boxStyle}>vehicle</div>
 <Xarrow
           
           start={box3Ref} //can be react ref
           end="four" //or an id
       />
             
            <div  className="third"id="third" style={boxStyle}>reservation</div>
            <div className="four"ref={box2Ref} style={boxStyle}>vehicle</div>
            <Xarrow
           
                start={box2Ref} 
                //can be react ref
                end="third" //or an id
            />

<div  className="second"id="second" ref={box3Ref}  style={boxStyle}>clerk</div>
<div  className="third"id="third" style={boxStyle}>reservation</div>
<Xarrow
           
           start={box3Ref} 
           //can be react ref
           end="third" 
           path="straight"
       />
            <div  className="second"id="second" ref={box3Ref}  style={boxStyle}>clerk</div>
            <div className="four"ref={box2Ref} style={boxStyle}>vehicle</div>
            <Xarrow
           
           start={box3Ref} 
           //can be react ref
           end="four" //or an id
       />
      
          </div> 
          <div className='dot'>.....</div>
          <div className='dot1'>.....</div>
          <div className="dot2">......</div>
          
      </div>
        {/* <div className='third'>
          vehicle
        </div>
        <div className='four'>
          reservation
        </div> */}
        
        
      <div className='side'>
   
          <p><pre>             Drag the labels from sidepanel </pre></p>
        <div className='div1' ref={pos} onMouseDown={handleMove}> 
  
       name
       </div>
       <div className='div5' ref={pos1} onMouseDown={handleMove1}>
        contractStatus
      </div>
      
      <div className='div2' ref={pos2} onMouseDown={handleMove2}>
        name        
      </div>
      <div className='div3' ref={pos3} onMouseDown={handleMove3}>
        +regnumber
      </div>
      <div className='div4' ref={pos4} onMouseDown={handleMove4}>
        reservationNumber
      </div>
      <div className='div6' ref={pos6} onMouseDown={handleMove6}>
        releaseDate
      </div>
     
    
    {/* </div> */}

 </div>
 <div className='head'> <b>Line Labels</b></div>
 <div className='div10' ref={pos7} onMouseDown={handleMove7}> 
  
  <i>+Gets keys from</i>
  </div>
<h4> Right occurences: greenBorder
  <br></br> wrong occurences:redBorder </h4>
 </div>
 </div>
  );
  
}
export default App;