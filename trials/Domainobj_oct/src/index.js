import React, { StrictMode } from "react";
import { createRoot } from "react-dom/client";
import "./styles.css";
import ReactDOM from "react-dom";
import App from "./App";
import { Line } from "react-lineto";

const root = createRoot(document.getElementById("root"));
root.render(
  <StrictMode>
   
    <App />
  </StrictMode>
);