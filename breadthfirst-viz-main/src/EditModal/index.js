import React from "react"
import { makeStyles, createStyles } from "@material-ui/core/styles"
import Backdrop from "@material-ui/core/Backdrop"
import { Button, Divider, Paper, TextField } from "@material-ui/core"
import Draggable from "react-draggable"

import Dialog from "@material-ui/core/Dialog"



const useStyles = makeStyles(theme =>
  createStyles({
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: "2px solid #495579",
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
      width: 400,
      [theme.breakpoints.down("sm")]: {
        width: "80vw"
      }
    },
    titleContainer: {
      padding: theme.spacing(0, 0, 3)
    },
    buttonContainer: {
      display: "flex",
      justifyContent: "flex-end"
    },
    deleteButton: {
      width: "8rem",
      backgroundColor: "#495579",
      color: "#ffffff",
      transition: "0.5s",
      border: `1px solid #495579`,
      "&:hover": {
        backgroundColor: "#ffffff",
        color: "#495579",
        border: `1px solid #495579`
      },
      marginRight: "0.5rem"
    },
    cancelButton: {
      width: "8rem",
      backgroundColor: "#ffffff",
      color: "#495579",
      transition: "0.5s",
      border: `1px solid #495579`,
      "&:hover": {
        backgroundColor: "#495579",
        color: "#ffffff"
      }
    },
    inputField: {
      padding: theme.spacing(2, 0, 2, 0),
      borderColor: theme.palette.primary.main
    },
    formControl: {
      margin: theme.spacing(1, 0, 3, 0),
      // minWidth: 120,
      width: "100%"
    },
    backdrop: {
      backgroundColor: "rgba(0, 0, 0, 0.2)"
    }
  })
)

function PaperComponent(props) {
  return (
    <Draggable
      handle="#draggable-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  )
}



const EditModal = React.memo(
  ({ editModalContent, setEditModalContent, handleConfirmButtonClick }) => {

    const classes = useStyles()
    const { label } = editModalContent?.data() ?? {}
   
    const [value, setValue] = React.useState( label )

    const handleClose = React.useCallback(() => {
      setEditModalContent()
    }, [setEditModalContent])


    function handleConfirmClick(event) {
      setValue(event.target.value)
      handleClose()
    }


    function handleInput(event) {
      setValue(event.target.value)
    }

   
    return (
      <Dialog
        aria-labelledby="edit-dialog"
        aria-describedby="edit-a-node-dialog"
        className={classes.modal}
        open={!!editModalContent}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
          className: classes.backdrop
        }}
      >
        <div className={classes.paper}>
          <div className={classes.titleContainer}>
            <h2 id="transition-modal-title">
              Edit node
            </h2>
            <Divider />
          </div>
          <TextField
            placeholder="Start Typing"
            variant="outlined"
            fullWidth
            autoFocus
            value={value}
            className={classes.inputField}
            onChange={handleInput}
          />

          <div className={classes.buttonContainer}>
            <Button
              onClick={handleConfirmClick}
              className={classes.deleteButton}
            >
              Submit
            </Button>
            <Button
              onClick={handleClose}
              className={classes.cancelButton}
              variant="outlined"
            >
              Cancel
            </Button>
          </div>
        </div>
      </Dialog>
    )
  }
)

export default EditModal
