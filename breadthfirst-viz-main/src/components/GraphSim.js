import React, {useEffect, useRef} from "react";
import cytoscape from "cytoscape";
import {option} from "../assets/option";
import QueueSim from "./QueueSim";
import Draggable from "react-draggable";
import DragIcon from "../assets/drag-indicator.svg";
import cxtmenu from "cytoscape-cxtmenu";
import { loadCSS } from "fg-loadcss";
import EditModal from "../EditModal";


cytoscape.use(cxtmenu);


const TBatchAction = {
    name: String,
    param: 
        {
            group: "nodes" | "edges",
            data: {
                id: String,
                source: String,
                target: String
            },
            renderedPosition: {
                x: Number,
                y: Number
            },
        }
};

const randomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const generateEdges = (graphData) => {
    option.elements.edges = [];

    for(let i = 0; i < graphData.noOfNodes+randomNumber(0,4); i++) {
        const a = randomNumber(1, graphData.noOfNodes);
        const b = randomNumber(1, graphData.noOfNodes);
        if (a !== b && !option.elements.edges.some(edge => {
            return edge.data.source === a && edge.data.target === b
        }))
        {
            option.elements.edges = [
                ...option.elements.edges,
                {
                    data: {
                        id: `${a}${b}`, weight: randomNumber(0, graphData.noOfNodes+4), source: `${a}`, target: `${b}`
                    }
                }
            ]
        }
    }
}



const GraphSim = ({ graphData, setGraphData }) => {
    const graphRef = useRef(null)
    const [log, setLog] = React.useState([]);
    const [edge, setEdge] = React.useState([])
    const [queue, setQueue] = React.useState([]);
    const [statusMessage, setStatusMessage] = React.useState({action: null, node: null});
    const [prevGraphData, setPrevGraphData] = React.useState({...graphData, noOfNodes: 3});


    const [openDialog, setOpenDialog] = React.useState({
    openLearn: null,
    openAdd: null
  });


  const handleOpenLearn = (ele) => {
    const data = ele.data();
    setOpenDialog({ ...openDialog, openLearn: true, nodeObj: data });
  };

  const urRef = React.useRef(null);

    const generateNodes = () => {
        option.elements.nodes = [{ data: { id: '1' , label: "Node 1" }}];

        if (graphData.noOfNodes > 1 && option.elements.nodes.length < graphData.noOfNodes)
            for(let i = 2; i <= graphData.noOfNodes; i++)
                option.elements.nodes = [...option.elements.nodes, { data: { id: `${i}` , label: `Node ${i}` }}]

        setGraphData({...graphData, flag: 1})
    }


    const divRef = useRef(null);

    useEffect(() => {
        divRef?.current?.scrollIntoView({ behavior: 'smooth' });
    });

    useEffect(() => {
        if (prevGraphData.noOfNodes !== graphData.noOfNodes)
            generateNodes();

        if (graphData.flag !== 3 && graphData.flag !== 5 && !graphData.randomize)
            option.elements.edges = [];

        if (graphData.randomize && prevGraphData.noOfNodes !== graphData.noOfNodes)
            generateEdges(graphData);

        drawGraph()

        setPrevGraphData(graphData)

    }, [graphData])

    useEffect(() => {
        if (edge.length === 2){
            option.elements.edges = [
                ...option.elements.edges,
                {
                    data: {
                        id: `${edge[0]}${edge[1]}`,
                        weight: randomNumber(0, graphData.noOfNodes+4),
                        source: `${edge[0]}`,
                        target: `${edge[1]}`
                    }
                }]
            drawGraph();
            setEdge([])
            setGraphData({...graphData, flag: 5})
        }
    },[edge])

    const [editModalContent, setEditModalContent] = React.useState();

    const drawGraph = () => {
        option.container = graphRef.current;
        const cy = cytoscape(option);
        

        cy.fit();
        


        const defaultsLevel = {
            menuRadius: function (ele) {
                return 70;
              }, // the outer radius (node center to the end of the menu) in pixels. It is added to the rendered size of the node. Can either be a number or function as in the example.
            selector: "node",
            //selector: "node", // elements matching this Cytoscape.js selector will trigger cxtmenus
            commands: [
              // an array of commands to list in the menu or a function that returns the array

              {
                content: '<span class="fas fa-pen fa-2x"></span>',
                select: (node) => {
                    setEditModalContent(node);
                },
                enabled: true // whether the command is selectable
              },
              
            ],

            fillColor: "rgba(0, 0, 0, 0.75)", // the background colour of the menu
            activeFillColor: "rgba(1, 105, 217, 0.75)", // the colour used to indicate the selected command
            activePadding: 8, // additional size in pixels for the active command
            indicatorSize: 24, // the size in pixels of the pointer to the active command, will default to the node size if the node size is smaller than the indicator size,
            separatorWidth: 3, // the empty spacing in pixels between successive commands
            spotlightPadding: 8, // extra spacing in pixels between the element and the spotlight
            adaptativeNodeSpotlightRadius: true, // specify whether the spotlight radius should adapt to the node size
            // minSpotlightRadius: 24, // the minimum radius in pixels of the spotlight (ignored for the node if adaptativeNodeSpotlightRadius is enabled but still used for the edge & background)
            // maxSpotlightRadius: 38, // the maximum radius in pixels of the spotlight (ignored for the node if adaptativeNodeSpotlightRadius is enabled but still used for the edge & background)
            openMenuEvents: "cxttapstart taphold", // space-separated cytoscape events that will open the menu; only `cxttapstart` and/or `taphold` work here
            itemColor: "white", // the colour of text in the command's content
            itemTextShadowColor: "transparent", // the text shadow colour of the command's content
            zIndex: 9999, // the z-index of the ui div
            atMouse: false, // draw menu at mouse position
            outsideMenuCancel: false // if set to a number, this will cancel the command if the pointer is released outside of the spotlight, padded by the number given
          };


          cy.cxtmenu(defaultsLevel);

          


        cy.on('tap', 'node', function(evt) {
            cy.$(`#${evt.target.id()}`).toggleClass('selected');
            
            setEdge(prev =>[...prev, evt.target.id()]);


        })
        
        

        let tempQ = [];
        let depth = 0;

        if (graphData.flag === 3) {
            const _actions = []
            setStatusMessage({action: null, node: null});

            const bfs = cy.elements().bfs({
                root: `#${graphData.startingNode}`,
                visit: (v, e, u, i, _depth) => {
                    console.log(v.id(),depth);
                    if (depth !== _depth) {
                        depth = _depth;
                        for (let i = 0; i < tempQ.length; i++) {
                            _actions.push({"action": "DEQUEUE", "node": tempQ[i]});
                        }
                        tempQ = [];
                    }

                    tempQ.push(v.id());
                    _actions.push({"action": "ENQUEUE", "node": v.id()})
                },
                directed: true
            });

            let i = 0;
            let __queue = []
            setQueue([])
            setLog([])

            const highlightNextElm = () => {

                if (i <= bfs.path.length) {
                    let _queue = []
                    let _log = []
                    let _status = {actions: null, node: null}

                    if (i === bfs.path.length) {
                        i++;
                        for (let j=0; j<__queue.length; j++) {
                            if(__queue[j].state==="ENQUEUED") {
                                __queue[j].state = "DEQUEUED";
                                setQueue(__queue)
                                setLog(prev => [...prev, {node: __queue[j].node, state: "DEQUEUED"}]);
                            }
                        }
                        _status = {action:'COMPLETE', node: null}
                    } else {
                        bfs?.path[i]?.addClass('highlighted');

                        for (let j = 0; j <= i; j++) {
                            let a = _actions[j]

                            if (a && typeof (a) !== 'undefined' && a.action === "ENQUEUE") {
                                _status = {action: 'ENQUEUED', node: a.node}
                                _queue.push({node: a.node, state: "ENQUEUED"})
                                _log.push({node: a.node, state: "ENQUEUED"})
                            } else if (a && typeof (a) !== 'undefined' && a.action === "DEQUEUE") {
                                _status = {action: 'DEQUEUED', node: a.node}
                                _log.push({node: a.node, state: "DEQUEUED"})
                                _queue.map((elm, k) => {
                                    if (elm.node === a.node) {
                                        _queue[k].state = "DEQUEUED"
                                    }
                                    return null;
                                })
                            }
                        }
                        i++;
                    }

                    setTimeout(() => {
                        if (_queue.length) {
                            __queue = _queue
                            setQueue(_queue)
                        }

                        if (_log.length) {
                            setLog(_log)
                        }

                        setStatusMessage(_status)
                        highlightNextElm()
                    }, 1000);
                }
            }
            highlightNextElm()
        }
    }

            urRef.current?.action(
            "changeData",
            ({ node , label }) => {
            // first, remove the nodes and all of its edges
            const removeActions =[]
            removeActions.push({
                name: "remove",
                param: node
            })
            removeActions.push({
                name: "remove",
                param: node.connectedEdges()
           
            }) 
            // then, re-add everything to graph with new data for node
            const addActions = []
            addActions.push({
                name: "add",
                param: {
                group: "nodes",
                data: { id: node.id() , label: label },
                position: node.position()
                // style: { 'background-color': 'darkgreen' },
                }
            })
            node.connectedEdges().forEach(edge => {
                addActions.push({
                name: "add",
                param: {
                    group: "edges",
                    data: {
                    id: edge.id(),
                    source: edge.source().id(),
                    target: edge.target().id()
                    }
                 }
                })
            })
            urRef.current.do("batch", [...removeActions, ...addActions])
          
            },
            () => urRef.current.undo()
        )


    const handleUpdateTitleOrType = React.useCallback(
        (node, newLabel) => {
        urRef.current.do("changeData", {
            node,
            newLabel
            
        });
    }, []);
   
    useEffect(() => {
        const fa = loadCSS("https://use.fontawesome.com/releases/v5.12.0/css/all.css", document.querySelector("#font-awesome-css"));
        return () => {
            fa.parentNode.removeChild(fa);
        };
    }, []);
    
    return (
        <>
            <div ref={graphRef} style={{width: '80%', height: '100vh'}} className="p-16 pr-32"/>
            <QueueSim queue={queue} current={statusMessage.node}
            />
            {(statusMessage.action !== null) && (
                <Draggable handle="#handle">
                    <div className="absolute rounded font-mono pr-8 pl-5 py-5 bottom-10 right-20 bg-gray-800 shadow text-white overflow-auto" style={{maxHeight: '400px'}}>
                        {log.map((item) => (
                            <div>
                                {`>  Node ${item?.node} is ${item?.state?.toLowerCase()}`}
                            </div>
                        ))}
                        {statusMessage.action === 'COMPLETE' && (
                            <>
                                <br/>
                                <br/>
                                Traversal Path -&nbsp;
                                {queue.map((elm) => (elm.node+" "))}
                                <br/>
                                Time Complexity - {queue.length-1 + queue.length}
                                <br/>
                                Space Complexity - {queue.length}
                                <br/>
                                <br/>
                                Traversal Complete
                                <br/>
                            </>
                        )}
                        <div ref={divRef} id="handle" className="pt-5">
                            <div className="bg-gray-600 absolute bottom-0 left-0 w-full items-center justify-center flex cursor-move">
                                <img src={DragIcon} style={{width:'20px', height:'20px', pointerEvents: 'none'}} alt="random" className="rotate-90 transform"/>
                            </div>
                        </div>
                    </div>
                </Draggable>
            )}
            <EditModal
        editModalContent={editModalContent}
        setEditModalContent={setEditModalContent}
        handleConfirmButtonClick={handleUpdateTitleOrType}
      />
        </>
    )
}

export default GraphSim;