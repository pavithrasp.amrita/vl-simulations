import React from "react";
import styled from "@emotion/styled";
import Responsively, {
  animation,
  background,
  border,
  clasification,
  dimension,
  flexbox,
  font,
  list,
  margin,
  other,
  overflow,
  padding,
  position,
  text,
  transform,
  transition
} from "responsively";

Responsively.configureBreakpoints([400, 500, 600, 700]);

const RStyles = styled("div")(
  ...animation,
  ...background,
  ...border,
  ...clasification,
  ...transition,
  ...margin,
  ...padding,
  ...text,
  ...dimension,
  ...flexbox,
  ...font,
  ...list,
  ...other,
  ...overflow,
  ...position,
  ...transform
);

/**
 * Responsive component
 */
const R = props => <RStyles {...props} />;

export default R;
