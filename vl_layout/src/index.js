import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import { R } from "./components";
import "./App.css";

function App() {
  return (
    <Fragment>
      <R
        fontFamily={["sans-serif"]}
        textAlign={["center"]}
        fontSize={[10, 14, 18, 24, 26]}
        color={["#000", null, null, "#fff"]}
        background={["#f9eeef", "#eab9bc", "#f2afb4", "#ef717c", "#f74b59"]}
        padding={["80px 10px"]}
        transition={["0.3s all ease-in-out"]}
      >
        <h1 style={{ margin: 0 }}>✨Hello Responsively ✨</h1>
      </R>
      <small style={{ fontFamily: "sans-serif" }}>
        📏 Resize view area to see `responsively` in action.
      </small>
    </Fragment>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
